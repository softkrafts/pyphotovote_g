#sample gunicorn script. This goes one level about the bitbucket checkout, so that it's a sibling of the top-level pyphotovote folder

#!/bin/bash

NAME="pyphotovote"                                  # Name of the application
DJANGODIR=/usr/local/share/photo_contest/pyphotovote/pyphotovote             # Django project directory
SOCKFILE=/usr/local/share/photo_contest/pyphotovote/pyphotovote/run/gunicorn.sock  # we will communicte using this unix socket
USER=photocontest                                       # the user to run as
GROUP=pyphotovote                                 # the group to run as
NUM_WORKERS=3                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=pyphotovote.settings             # which settings file should Django use
DJANGO_WSGI_MODULE=pyphotovote.wsgi                     # WSGI module name

echo "Starting $NAME"

# Activate the virtual environment
cd $DJANGODIR
source /usr/local/share/photo_contest/virtualenv/pyphotovote/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /usr/local/share/photo_contest/virtualenv/pyphotovote/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --log-level=debug \
  --bind=unix:$SOCKFILE

