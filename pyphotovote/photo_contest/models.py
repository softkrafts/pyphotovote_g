from __future__ import division
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from dateutil.relativedelta import relativedelta
from os import path


class Contest(models.Model):
    name = models.CharField(max_length=1024)
    submission_start_date = models.DateTimeField(default=datetime(year=datetime.today().year,
                                                                  month=datetime.today().month,
                                                                  day=datetime.today().day))
    submission_end_date = models.DateTimeField(default=datetime(year=(datetime.today() + relativedelta(days=7)).year,
                                                                month=(datetime.today() + relativedelta(days=7)).month,
                                                                day=(datetime.today() + relativedelta(days=7)).day,))
    voting_start_date = models.DateTimeField(default=datetime(year=(datetime.today() + relativedelta(days=7)).year,
                                                                month=(datetime.today() + relativedelta(days=7)).month,
                                                                day=(datetime.today() + relativedelta(days=7)).day,))
    voting_end_date = models.DateTimeField(default=datetime(year=(datetime.today() + relativedelta(days=14)).year,
                                                                month=(datetime.today() + relativedelta(days=14)).month,
                                                                day=(datetime.today() + relativedelta(days=14)).day,))
    subfolder = models.CharField(max_length=1024)
    max_photos_per_user = models.IntegerField(default=10)
    photos_per_battle = models.IntegerField(default=2)
    results_chart_count = models.IntegerField(default=10)
    winners_count = models.IntegerField(default=5)
    minimum_shortest_dimension = models.IntegerField(default=1650) #1650x2100 can be printed at 11x14 inches
    maximum_file_size_MB = models.IntegerField(default=25)
    results_histogram_step_size = models.IntegerField(default=10)

    def __unicode__(self):
        return '%s (%s)' %(self.name, self.subfolder)


def get_upload_folder(instance, filename):
    return path.join(instance.contest.subfolder, filename)


class Photo(models.Model):

    contest = models.ForeignKey(Contest)
    photo_file = models.ImageField(upload_to=get_upload_folder)
    thumbnail_file_100 = models.CharField(max_length=1024)
    thumbnail_file_200 = models.CharField(max_length=1024)
    thumbnail_file_300 = models.CharField(max_length=1024)
    thumbnail_file_1024 = models.CharField(max_length=1024)
    photographer = models.ForeignKey(User)

    @property
    def victory_count(self):
        return len(self.victories.all())

    @property
    def battle_count(self):
        return len(self.battle_set.all())

    @property
    def victory_ratio(self):
        if self.battle_count > 0:
            return self.victory_count / self.battle_count
        else:
            return 0

    def __unicode__(self):
        return '%s (%s)' %(str(self.photo_file.name), self.photographer)


class Battle(models.Model):
    winner = models.ForeignKey(Photo, related_name='victories', blank=True, null=True)
    competitors = models.ManyToManyField(Photo)
    voter = models.ForeignKey(User, blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    contest = models.ForeignKey(Contest)

    def __unicode__(self):
        if self.winner:
            return str(self.winner.photo_file)
        else:
            return '(no winner for battle %s)' %self.id


class Log(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    contest = models.ForeignKey(Contest)
    message = models.CharField(max_length=1024)

    def __unicode__(self):
        return '%s (%s): %s' % (self.timestamp, self.user, self.message)