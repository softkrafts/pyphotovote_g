from django import forms
from models import Photo

class PhotoUploadForm(forms.ModelForm):
    photo_file = forms.ImageField(label='Submit a photo')
    class Meta:
        model = Photo
        fields = ['photo_file']

class VotingForm(forms.Form):
    winner_id = forms.IntegerField(widget=forms.HiddenInput)
    battle_id = forms.IntegerField(widget=forms.HiddenInput)