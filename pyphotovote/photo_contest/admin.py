from django.contrib import admin
from models import Photo, Battle, Contest, Log

admin.site.register(Contest)
admin.site.register(Photo)
admin.site.register(Battle)
admin.site.register(Log)
