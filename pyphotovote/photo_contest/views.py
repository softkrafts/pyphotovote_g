from django.contrib.auth.decorators import login_required
from django.db.models import Count, Min
from django.forms.formsets import formset_factory
from django.shortcuts import render, redirect
from forms import PhotoUploadForm, VotingForm
from models import Photo, Battle, Contest, Log
from datetime import datetime
from django.utils import timezone
from PIL import Image
from django.core.exceptions import MultipleObjectsReturned
from django.contrib.auth.models import User
from django.contrib import messages
import random, os


def get_contest(contest_id):
    return Contest.objects.get(id=contest_id)


def log(user, contest_id, message):
    contest = get_contest(contest_id)
    new_log = Log(user=user, contest=contest, message=message)
    new_log.save()


def get_now():
    return timezone.make_aware(datetime.now(), timezone.get_default_timezone())


def generate_thumbnail(contest, photo, max_dimension):
    base_folder, base_name = os.path.split(photo.photo_file.path)
    thumbnail_folder_base = str(max_dimension) + 'x' + str(max_dimension)
    thumbnail_folder = os.path.join(base_folder, thumbnail_folder_base)
    if not os.path.exists(thumbnail_folder):
        os.makedirs(thumbnail_folder)
    thumbnail_path = os.path.join(thumbnail_folder, base_name)
    image = Image.open(photo.photo_file.path)
    image.thumbnail((max_dimension, max_dimension))
    image.save(thumbnail_path)
    return os.path.join(contest.subfolder, thumbnail_folder_base, base_name)


def submissions_get(contest, context, request, template):
    existing_submissions_for_user = Photo.objects.filter(photographer=request.user, contest=contest)
    photo_upload_formset, photo_upload_form_count = get_photo_upload_formset_factory(contest, existing_submissions_for_user.count())
    context['existing_submissions_for_user'] = existing_submissions_for_user
    context['upload_forms'] = photo_upload_formset
    context['can_submit'] = photo_upload_form_count > 0
    return render(request, template, context)


def resolution_is_valid(contest, photo):
    image = Image.open(photo.photo_file)
    width, height = image.size
    smallest_dimension = min(width, height)
    return smallest_dimension >= contest.minimum_shortest_dimension, width, height


def filesize_is_valid(contest, photo):
    maximum_file_size_bytes = contest.maximum_file_size_MB * 1024 * 1024
    return photo.photo_file.size <= maximum_file_size_bytes, photo.photo_file.size / 1024.0 / 1024.0


def submission_count_is_less_than_max(user, contest):
    existing_submissions_for_user = Photo.objects.filter(photographer=user, contest=contest)
    return existing_submissions_for_user.count() < contest.max_photos_per_user


def process_valid_submissions_form(contest, context, photo_upload_form, request):
    photo_file = photo_upload_form.save(commit=False)
    if photo_file.photo_file:
        meets_resolution, width, height = resolution_is_valid(contest, photo_file)
        if meets_resolution:
            meets_filesize, filesize_MB = filesize_is_valid(contest, photo_file)
            if meets_filesize:
                meets_submission_count = submission_count_is_less_than_max(request.user, contest)
                if meets_submission_count:
                    photo_file.photographer = request.user
                    photo_file.contest = contest
                    photo_file.save()
                    photo_file.thumbnail_file_100 = generate_thumbnail(contest, photo_file, 100)
                    photo_file.thumbnail_file_200 = generate_thumbnail(contest, photo_file, 200)
                    photo_file.thumbnail_file_300 = generate_thumbnail(contest, photo_file, 300)
                    photo_file.thumbnail_file_1024 = generate_thumbnail(contest, photo_file, 1024)
                    photo_file.save()
                    return True, get_original_filename_from_photo_upload_form(photo_upload_form) + ' has been submitted.'
                else:
                    return False, 'You have already submitted the maximum number of photos for this contest.'
            else:
                return False, photo_file.photo_file.name + ' does not meet the maximum file size for this contest. Your file can be at most ' + str(contest.maximum_file_size_MB) + ' MB. Your file is ' + str(round(filesize_MB, 2)) + ' MB.'
        else:
            return False, photo_file.photo_file.name + ' does not meet the minimum dimensions required for this contest. The shortest side of your image should be at least ' + str(contest.minimum_shortest_dimension) + ' pixels. Your image is ' + str(width) + 'x' + str(height) + '.'
    else:
        if photo_file.photo_file.name:
            return False, photo_file.photo_file.name + ' is not a valid image file.'
        else:
            return False, ''


def get_original_filename_from_photo_upload_form(photo_upload_form):
    return photo_upload_form.files[photo_upload_form.prefix + '-photo_file'].name

def process_submissions_form(contest, context, photo_upload_form, request):
    existing_submissions_for_user = Photo.objects.filter(photographer=request.user, contest=contest)
    context['existing_submissions_for_user'] = existing_submissions_for_user

    photo_upload_formset, photo_upload_form_count = get_photo_upload_formset_factory(contest, existing_submissions_for_user.count())
    context['upload_forms'] = photo_upload_formset
    context['can_submit'] = photo_upload_form_count > 0
    if photo_upload_form.is_valid():
        return process_valid_submissions_form(contest, context, photo_upload_form, request)
    else:
        return False, get_original_filename_from_photo_upload_form(photo_upload_form) + ' failed validation. ' + photo_upload_form.errors['photo_file'][0]


def submissions_post(contest, context, request, template):
    context['success_failure_messages'] = []
    PhotoUploadFormSet, photo_upload_form_count = get_photo_upload_formset_factory(contest)
    photo_upload_formset = PhotoUploadFormSet(request.POST, request.FILES)
    context['can_submit'] = photo_upload_form_count > 0
    for photo_upload_form in photo_upload_formset.forms:
        submission_succeeded, success_failure_message = process_submissions_form(contest, context, photo_upload_form, request)
        context['success_failure_messages'].append(success_failure_message)
    return render(request, template, context)


@login_required
def submissions(request, contest_id, template='photo_contest/submissions.html'):
    context = {}
    context['contest_id'] = contest_id
    contest = get_contest(contest_id)
    context['max_photos_per_user'] = contest.max_photos_per_user
    now = get_now()
    if now >= contest.submission_start_date and now < contest.submission_end_date:
        if request.method == 'POST':
            return submissions_post(contest, context, request, template)
        else:
            return submissions_get(contest, context, request, template)
    elif now >= contest.voting_start_date and now < contest.voting_end_date:
        return redirect('vote', contest_id)
    elif now < contest.voting_start_date:
        return redirect('/', contest_id)
    else:
        return redirect('results', contest_id)


def get_photo_upload_formset_factory(contest, existing_submissions_for_user=0):
    remaining_photos_per_user = contest.max_photos_per_user - existing_submissions_for_user
    photo_upload_formset_factory = formset_factory(PhotoUploadForm, extra=remaining_photos_per_user)
    return photo_upload_formset_factory, remaining_photos_per_user


def setup_battle(contest, photos, voter):
    battle = Battle(contest=contest)
    battle.save()
    battle.competitors = photos
    battle.voter = voter
    battle.save()
    return battle


def setup_photo_forms(battle, photos):
    photo_forms = []
    for photo in photos:
        form = VotingForm({'winner_id': photo.id, 'battle_id': battle.id})
        photo_forms.append((photo, form))
    return photo_forms


def get_voting_context(photo_forms, contest_id):
    context = {}
    context['photo_forms'] = photo_forms
    context['contest_id'] = contest_id
    return context


@login_required
def vote(request, contest_id, template='photo_contest/vote.html'):
    contest = Contest.objects.get(id=contest_id)
    now = get_now()
    if now >= contest.voting_start_date and now < contest.voting_end_date:
        photos = get_photos(contest)
        battle = setup_battle(contest, photos, request.user)
        photo_forms = setup_photo_forms(battle, photos)
        context = get_voting_context(photo_forms, contest_id)

        return render(request, template, context)
    else:
        return redirect('results', contest_id)


def save_winner(request, battle_id, winner_id, contest_id):
    try:
        battle = Battle.objects.get(id=battle_id)
    except Battle.DoesNotExist:
        messages.error(request, 'This battle (id=%d) does not exist.' % (battle_id))
        log(request.user,
            contest_id,
            'battle invalid: battle_id=%d does not exist' % (battle_id))
        return

    if battle.winner is None:
        competitors = [competitor.id for competitor in battle.competitors.all()]
        if winner_id in competitors:
            if battle.voter == request.user:
                battle.winner = Photo.objects.get(id=winner_id)
                battle.timestamp = get_now()
                battle.save()
            else:
                messages.error(request,
                               'The expected voter for this battle (id=%d) is not %s.' % (battle_id, request.user))
                log(request.user,
                    contest_id,
                    'voter mismatch: got user=%s, expected user=%s, battle_id=%d' % (request.user,
                                                                                     battle.voter,
                                                                                     battle_id))

        else:
            messages.error(request,
                           'Winner %d was not one of the competitors in this battle (id=%d).' % (winner_id, battle_id))
            log(request.user,
                contest_id,
                'winner mismatch: got winner=%d, competitors=%s, battle_id=%d' % (winner_id,
                                                                                  competitors,
                                                                                  battle_id))
    else:
        messages.error(request,
                       'A winner has already been voted on by %s for this battle (id=%d).' % (battle.voter, battle_id))
        log(request.user,
            contest_id,
            'winner exists: existing winner=%d, submitted=%d, battle_id=%d, voter=%s' % (battle.winner.id,
                                                                                         winner_id,
                                                                                         battle_id,
                                                                                         battle.voter))


def get_battle_outcome(form):
    battle_id = form.cleaned_data['battle_id']
    winner_id = form.cleaned_data['winner_id']
    return battle_id, winner_id


@login_required
def record_vote(request, contest_id):
    if request.method == 'POST':
        form = VotingForm(request.POST)
        if form.is_valid():
            battle_id, winner_id = get_battle_outcome(form)
            save_winner(request, battle_id, winner_id, contest_id)
    return redirect('vote', contest_id)


def get_least_battle_count(contest):
    min_battles_query = Photo.objects.filter(contest=contest).annotate(battle_count=Count('battle')).aggregate(
        least_battles=Min('battle_count'))
    if not min_battles_query['least_battles']:
        min_battles_query['least_battles'] = 0
    least_battle_count = min_battles_query['least_battles']
    return least_battle_count


def get_low_battle_count_photos(least_battle_count, contest):
    photos = Photo.objects.filter(contest=contest).annotate(num_battles=Count('battle')).filter(
        num_battles__lte=least_battle_count)
    photos = list(photos)
    return photos


def extend_with_random_photos(photo_count, contest, photos):
    photo_deficit = contest.photos_per_battle - photo_count
    if photo_deficit > 0:
        selected_photo_ids = []
        for photo in photos:
            selected_photo_ids.append(photo.id)
        photos.extend(Photo.objects.filter(contest=contest).exclude(id__in=selected_photo_ids).order_by('?')[:photo_deficit])


def get_photos(contest):
    least_battle_count = get_least_battle_count(contest)
    photos = get_low_battle_count_photos(least_battle_count, contest)
    photo_count = len(photos)
    extend_with_random_photos(photo_count, contest, photos)
    return pick_photos_for_battle(photos, contest)

def get_random_photo(photos, selected_photos):
    number_of_photos = len(photos)
    photo_index = random.randint(0, number_of_photos - 1)
    if photos[photo_index] not in selected_photos:
        return photos[photo_index]
    else:
        return None


def pick_photos_for_battle(photos, contest):
    number_of_photos = len(photos)
    if number_of_photos < contest.photos_per_battle:
        return photos

    selected_photos = set([])

    while len(selected_photos) < contest.photos_per_battle:
        random_photo = get_random_photo(photos, selected_photos)
        if random_photo:
            selected_photos.add(random_photo)

    return selected_photos


def get_photos_ordered_by_victory_ratio(contest_id):
    contest = get_contest(contest_id)
    photos = list(Photo.objects.filter(contest=contest))
    photos.sort(key=lambda x: x.victory_ratio, reverse=True)
    return photos


def get_participation_venn_svg(photographers, voters):
    import matplotlib
    matplotlib.use('Agg')
    import StringIO
    from matplotlib_venn import venn2
    figure = matplotlib.pyplot.figure()
    matplotlib.pyplot.xkcd()
    figure.suptitle('User Participation')

    if len(photographers) > 0 and len(voters) > 0:
        matplotlib.pyplot.xkcd()
        photographer_voter_count = len(set(photographers).intersection(voters))
        photographer_only_count = len(photographers) - photographer_voter_count
        voter_only_count = len(voters) - photographer_voter_count
        venn2(subsets=(photographer_only_count,
                       voter_only_count,
                       photographer_voter_count),
              set_labels=('Photographers', 'Voters'))

    image = StringIO.StringIO()
    figure.savefig(image, format='svg')
    image.seek(0)
    return image.buf


def get_votes_per_voter_histogram_svg(voters, step_size=10):
    import matplotlib
    matplotlib.use('Agg')
    import StringIO

    figure = matplotlib.pyplot.figure()
    matplotlib.pyplot.xkcd()
    figure.suptitle('How many times do users vote?')

    if len(voters) > 0:
        x = []
        for voter in voters:
            x.append(len(voter.battle_set.all()))

        matplotlib.pyplot.hist(x,
                               bins=range(0, max(x) + step_size, step_size),
                               facecolor='#C77966')

    matplotlib.pyplot.ylabel('Number of Users')
    matplotlib.pyplot.xlabel('Number of Votes')

    image = StringIO.StringIO()
    figure.savefig(image, format='svg')
    image.seek(0)
    return image.buf


def get_battle_timestamp_histogram_svg(contest, battles):
    import matplotlib
    matplotlib.use('Agg')
    import StringIO
    from datetime import timedelta

    figure = matplotlib.pyplot.figure()
    matplotlib.pyplot.xkcd()
    figure.suptitle('When do users vote?')

    if len(battles) > 0:
        x = []
        for battle in battles:
            if battle.timestamp:
                x.append(timezone.localtime(battle.timestamp))

        time_list = [(t - contest.voting_start_date).total_seconds() for t in x]
        matplotlib.pyplot.hist(time_list, facecolor='#C77966')

        locs, labels = matplotlib.pyplot.xticks()
        for index, loc in enumerate(locs):
            tick_timestamp = contest.voting_start_date + timedelta(0, loc)
            tick_timestamp = timezone.localtime(tick_timestamp)
            labels[index] = tick_timestamp.strftime("%Y-%m-%d %H:%M")

        matplotlib.pyplot.xticks(locs, labels, rotation=10)

        xticklabels = figure.axes[0].get_xticklabels()
        for xticklabel in xticklabels:
            xticklabel.set_fontsize(6)

    matplotlib.pyplot.ylabel('Number of Votes')
    matplotlib.pyplot.xlabel('Date/Time')

    image = StringIO.StringIO()
    figure.savefig(image, format='svg')
    image.seek(0)
    return image.buf


def get_contest_stats(contest_id):
    contest = get_contest(contest_id)

    photographers = User.objects.filter(photo__contest=contest).distinct()
    voters = User.objects.filter(battle__competitors__contest=contest).distinct()
    winners = User.objects.filter(photo__contest=contest).filter(photo__victories__isnull=False).distinct()
    battles = Battle.objects.filter(contest=contest)

    return {
        'photographers': photographers,
        'voters': voters,
        'winners': winners,
        'all_participants': set(photographers).union(voters),
        'participation_venn_svg': get_participation_venn_svg(photographers, voters),
        'votes_per_voter_histogram_svg': get_votes_per_voter_histogram_svg(voters, contest.results_histogram_step_size),
        'battle_timestamp_histogram_svg': get_battle_timestamp_histogram_svg(contest, battles),
    }

def get_results_context(photos, contest_id, top=[], winners=[]):
    context = {}
    context['photos'] = photos
    context['top'] = top
    context['winners'] = winners
    context['contest_id'] = contest_id
    context['stats'] = get_contest_stats(contest_id)
    return context


def results(request, contest_id, template='photo_contest/results.html'):
    contest = get_contest(contest_id)
    photos = get_photos_ordered_by_victory_ratio(contest_id)
    context = get_results_context(photos,
                                  contest_id,
                                  photos[0:contest.results_chart_count],
                                  photos[0:contest.winners_count],
                                  )
    return render(request, template, context)


def contests(request, template='photo_contest/contests.html'):
    context = {}
    now = get_now()

    contests_all = Contest.objects.all()
    context['contests_all'] = contests_all

    contests_pre_submissions = Contest.objects.filter(submission_start_date__gt=now)
    context['contests_pre_submissions'] = contests_pre_submissions

    contests_in_submissions = Contest.objects.filter(submission_start_date__lte=now, submission_end_date__gt=now)
    context['contests_in_submissions'] = contests_in_submissions

    contests_in_voting = Contest.objects.filter(voting_start_date__lte=now, voting_end_date__gt=now)
    context['contests_in_voting'] = contests_in_voting

    contests_closed = Contest.objects.filter(voting_end_date__lt=now)
    context['contests_closed'] = contests_closed

    return render(request, template, context)


def gallery(request, contest_id, template='photo_contest/gallery.html'):
    context = {}
    contest = get_contest(contest_id)
    photos = Photo.objects.filter(contest=contest)
    context['photos'] = photos
    return render(request, template, context)


@login_required
def photo_delete(request, photo_id):
    if request.method == 'POST':
        try:
            photo = Photo.objects.get(id=photo_id)
            if photo.photographer == request.user:
                image_files = [photo.photo_file.path]
                base_folder, base_name = os.path.split(photo.photo_file.path)
                root_folder, base_folder = os.path.split(base_folder)
                for thumbnail_file in (photo.thumbnail_file_100, photo.thumbnail_file_200, photo.thumbnail_file_300, photo.thumbnail_file_1024):
                    image_files.append(os.path.join(root_folder, thumbnail_file))
                photo.delete()
                for image_file in image_files:
                    os.remove(image_file)
        except (MultipleObjectsReturned, Photo.DoesNotExist, OSError) as exception:
            return redirect(request.POST['next'])
        return redirect(request.POST['next'])
    else:
        return redirect('contests')
