from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'photo_contest.views.contests', name='contests'),
    url(r'submissions/(\d+)/$', 'photo_contest.views.submissions', name='submissions'),
    url(r'voting/(\d+)/$', 'photo_contest.views.vote', name='vote'),
    url(r'record_vote/(\d+)/$', 'photo_contest.views.record_vote', name='record_vote'),
    url(r'results/(\d+)/$', 'photo_contest.views.results', name='results'),
    url(r'gallery/(\d+)/$', 'photo_contest.views.gallery', name='gallery'),
    url(r'photo/delete/(\d+)/$', 'photo_contest.views.photo_delete', name='photo_delete'),
)
