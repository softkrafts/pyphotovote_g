$(document).ready(function(){
    setMaxWidth();
    $( window ).bind( "resize", setMaxWidth );

    function setMaxWidth() {
        $('.photo').each(
            function(){
                $(this).css("max-width", ( document.documentElement.clientWidth / 2 - 50) + 'px');
                $(this).css("max-height", ( document.documentElement.clientHeight - 150) + 'px');
            }
        );
    }
});
